﻿USE productosclientes;

-- ejercicio 2

  -- Crear una vista que nos permita ver los productos cuyo precio es mayor que 100. Mostrar los campos CODIGO ARTICULO,
-- SECCION Y PRECIO. Guardar la vista como CONSULTA1.


CREATE OR REPLACE VIEW consulta1 AS 
  SELECT 
    p.`CÓDIGO ARTÍCULO`, p.SECCIÓN, p.PRECIO  
    FROM productos p 
    WHERE (p.PRECIO > 100) ;

SELECT * FROM consulta1;


-- Ejercicio 3
 -- La vista anterior seria la siguiente, Modificarla para ordenar por precio de forma descendente

CREATE OR REPLACE VIEW consulta1
  AS 
  SELECT 
  p.`CÓDIGO ARTÍCULO`,
  p.SECCIÓN,  
  p.PRECIO
  FROM 
  productos p
  WHERE (p.PRECIO > 100)
    ORDER BY PRECIO;

SELECT * FROM consulta1 c;

  -- ejercicio 4

  -- Crear una vista que utilice como tabla la vista CONSULTA1 y que nos muestre todos productos cuya sección es DEPORTES.
--    Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA2.

CREATE OR REPLACE VIEW consulta2 AS
  SELECT 
    p.`CÓDIGO ARTÍCULO`, 
    p.SECCIÓN, 
    p.PRECIO  
    FROM consulta1 p 
    WHERE(p.SECCIÓN= 'DEPORTES');

SELECT * FROM consulta2 c;



-- ejercicio 5
-- La vista anterior seria la siguiente

  SELECT 
c1.`CÓDIGO ARTÍCULO`, c1.SECCIÓN, c1.PRECIO
    FROM consulta1 c1  
    WHERE (c1. `SECCIÓN`= 'deportes');


INSERT INTO consulta1 
VALUES ('AR90', 'NOVEDADES', 5  );

SELECT * FROM productos p;







-- ejercicio 6
  -- modificar la vista consulta1 y activar el check en la vista en moda local.



  CREATE OR REPLACE VIEW consulta1
  AS 
  SELECT 
  p.`CÓDIGO ARTÍCULO`,
  p.SECCIÓN,  
  p.PRECIO
  FROM 
  productos p
  WHERE (p.PRECIO > 100)
    ORDER BY PRECIO
  WITH LOCAL CHECK OPTION;


-- ejercicio 7 

INSERT INTO consulta1 
VALUES ('AR91', 'NOVEDADES', 5  );

SELECT * FROM productos p;

-- EJERCICIO 8

INSERT INTO consulta2
VALUES ('AR92', 'NOVEDADES', 5  );

SELECT * FROM productos p;

-- EJERCICIO 9 


CREATE OR REPLACE VIEW consulta2 AS
  SELECT 
    p.`CÓDIGO ARTÍCULO`, 
    p.SECCIÓN, 
    p.PRECIO  
    FROM consulta1 p 
    WHERE(p.SECCIÓN= 'DEPORTES')
  WITH LOCAL CHECK OPTION;

INSERT INTO consulta2
VALUES ('AR93', 'DEPORTES', 5  );


SELECT * FROM productos p;





-- EJERCICIO 10 CON CASCADE

  CREATE OR REPLACE VIEW consulta2 AS
  SELECT 
    p.`CÓDIGO ARTÍCULO`, 
    p.SECCIÓN, 
    p.PRECIO  
    FROM consulta1 p 
    WHERE(p.SECCIÓN= 'DEPORTES')
  WITH CASCADED CHECK OPTION;

-- EJERCICIO 11

INSERT INTO consulta2
VALUES ('AR94', 'DEPORTES', 200  );


SELECT * FROM productos p;


